package service

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"time"
	"xzs/common"
	"xzs/config"
	"xzs/model"
	"xzs/model/entity"
	"xzs/model/request"
	"xzs/pkg/encryptutil"
	"xzs/pkg/jwtutil"
)

var CookieExpireDuration int = 3600 * 24 * 7

func LoginService(c *gin.Context, req request.LoginRequest) common.RestResponse {
	res := common.RestResponse{
		Code:    common.Ok,
		Message: common.Ok.Msg(),
	}
	// 登录逻辑
	user, err := model.FindUserByUserName(req.UserName)
	if err != nil {
		res.Code = common.AuthError
		res.Message = common.AuthError.Msg()
		return res
	}
	// 判断密码是否正确
	pwd, err := encryptutil.RsaDecode(user.Password)
	if err != nil || req.Password != pwd {
		res.Code = common.AuthError
		res.Message = common.AuthError.Msg()
		return res
	}
	// 判断用户状态
	if user.Status == 2 {
		res.Code = common.AuthError
		res.Message = "用户被禁用"
		return res
	}

	// 登录成功设置session
	session := sessions.Default(c)
	session.Set("userName", req.UserName)
	err = session.Save()
	if err != nil {
		res.Code = common.AuthError
		res.Message = "设置登录状态错误"
		return res
	}
	// 判断是否需要设置cookie
	if req.Remember {
		token, _, err := jwtutil.GenToken(req.UserName)
		if err != nil {
			res.Code = common.AuthError
			res.Message = "记住密码设置失败"
			return res
		}
		c.SetCookie("token", token, CookieExpireDuration, "/", "localhost", false, true)
	}
	userEventLog := entity.UserEventLog{
		UserId:     user.Id,
		UserName:   user.UserName,
		RealName:   user.RealName,
		CreateTime: time.Now(),
		Content:    user.UserName + "  登录了学之思开源考试系统",
	}
	// 添加登录日志
	config.GlobalPool.Submit(func() {
		model.AddUserEventLog(&userEventLog)
	})
	newUser := entity.User{
		UserName:  user.UserName,
		ImagePath: user.ImagePath,
	}
	res.Response = newUser
	return res
}

func LogoutService(c *gin.Context) {
	// 清除session
	session := sessions.Default(c)
	session.Clear()
	session.Save()
	// 清除cookie
	c.SetCookie("token", "", -1, "/", "localhost", false, true)
	userName := c.GetString("userName")
	// 记录日志
	config.GlobalPool.Submit(func() {
		if userName == "" {
			return
		}
		user, err := model.FindUserByUserName(userName)
		if err != nil {
			return
		}
		userEventLog := entity.UserEventLog{
			UserId:     user.Id,
			UserName:   user.UserName,
			RealName:   user.RealName,
			CreateTime: time.Now(),
			Content:    user.UserName + "  登出了学之思开源考试系统",
		}
		err = model.AddUserEventLog(&userEventLog)
	})
}

func UserUpdateService(userName string, req request.UserUpdateRequest) (err error) {
	user := entity.User{
		RealName:   req.RealName,
		Phone:      req.Phone,
		ModifyTime: time.Now(),
	}
	err = model.UpdateUserByUserName(userName, user)
	return
}

func UserDeleteService(id int) (err error) {
	return model.DeleteUserById(id)
}
